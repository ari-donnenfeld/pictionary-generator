import 'package:flutter/material.dart';
import 'dart:async';


class CanvasPainting extends StatefulWidget {
    Stopwatch stopwatch;

    CanvasPainting(Stopwatch stopwatch) : stopwatch = stopwatch {}

    @override
        _CanvasPaintingState createState() => _CanvasPaintingState();
}

class TouchPoints {
    Paint paint;
    Offset points;
    bool end;
    TouchPoints({required this.points, required this.paint, this.end = false});
}

class Stroke {
    Paint paint;
    Path path;
    Stroke({required this.paint, required this.path});
}

class _CanvasPaintingState extends State<CanvasPainting> {

    // List of strokes
    List<Stroke> strokes = [];
    // Currently Drawn Stroke / Point
    Stroke currentStroke = Stroke(paint: Paint(), path: Path());

    StrokeCap strokeType = StrokeCap.round;
    double strokeWidth = 8.0;
    Color selectedColor = Colors.black;


    String sand = "";
    Timer? timer;



    void _updatesand() {
        setState(() {
            int miliseconds = widget.stopwatch.elapsedMilliseconds;
            String minutes = (miliseconds ~/ 60000).toString();
            String seconds = (miliseconds ~/ 1000 % 60).toString();
            sand = "$minutes:${seconds.padLeft(2, '0')}";
        });
    }

    @override
    void dispose() {
        timer?.cancel();
        super.dispose();
    }


    @override
    Widget build(BuildContext context) {
        // Setup Relative Sizes
        double screenWidth = MediaQuery.of(context).size.width;
        double screenHeight = MediaQuery.of(context).size.height;
        double buttonSize = screenWidth/10;
        strokeWidth = screenWidth * 0.015;
        debugPrint("Redrawing...");


        if (widget.stopwatch.isRunning && timer == null) {
            timer = Timer.periodic(const Duration(seconds: 1), (_) => _updatesand() );
        }


        return Scaffold(
            appBar: AppBar(
                title: Text("Pictionary", style: TextStyle(fontSize: screenWidth * 0.06)),
                centerTitle: true,
                leadingWidth: screenWidth * 0.15,
                leading: Listener(
                    onPointerDown: (details) {
                        Navigator.pop(context);
                    },
                    child: Container(
                        // Without the color the hit area is much smaller
                        // idk why
                        color: Colors.transparent,
                        child: Icon(Icons.arrow_back, size: screenWidth * 0.07)
                    )
                ),

                toolbarHeight: screenHeight * 0.08
            ),
            body: Stack(
                children: [
                    Positioned(
                        top: screenWidth * 0.05,
                        right: screenWidth * 0.05,
                        child: Text(sand, style: TextStyle(color: Colors.grey, fontSize: screenWidth * 0.06, fontWeight: FontWeight.bold)),
                    ),
                    GestureDetector(
                        onTapDown: (details) {
                            debugPrint("onTapDown");
                            // Set single point
                            // If cancelled (a pan not a touch)
                            // then it still counts
                            setState(() {
                                RenderBox renderBox = context.findRenderObject() as RenderBox;
                                currentStroke = Stroke(
                                        paint :Paint()
                                        ..strokeCap = strokeType
                                        ..isAntiAlias = true
                                        ..color = selectedColor
                                        ..style = PaintingStyle.stroke
                                        ..strokeWidth = strokeWidth,
                                        path: Path());
                                currentStroke.path.moveTo(details.localPosition.dx, details.localPosition.dy);
                                
                            });
                        },
                        onTapUp: (details) {
                            debugPrint("onTapUp");
                            // if touch not cancelled (definitely a tap)
                            // Just override the current path and add just one element
                            setState(() {
                                RenderBox renderBox = context.findRenderObject() as RenderBox;
                                currentStroke = Stroke(
                                        paint :Paint()
                                        ..strokeCap = strokeType
                                        ..isAntiAlias = true
                                        ..color = selectedColor
                                        ..style = PaintingStyle.fill
                                        ..strokeWidth = strokeWidth,
                                        path: Path());
                                currentStroke.path.moveTo(details.localPosition.dx, details.localPosition.dy);
                                currentStroke.path.addOval(
                                    Rect.fromCircle(
                                        center: Offset(details.localPosition.dx, details.localPosition.dy),
                                        radius: strokeWidth/1.9
                                    )
                                );
                                strokes.add(currentStroke);
                            });
                        },
                        onPanStart: (details) {
                            debugPrint("onPanStart");
                            setState(() {
                                RenderBox renderBox = context.findRenderObject() as RenderBox;
                                currentStroke = Stroke(
                                        paint :Paint()
                                        ..strokeCap = strokeType
                                        ..isAntiAlias = true
                                        ..color = selectedColor
                                        ..style = PaintingStyle.stroke
                                        ..strokeWidth = strokeWidth,
                                        path: Path());
                                currentStroke.path.moveTo(details.localPosition.dx, details.localPosition.dy);
                                strokes.add(currentStroke);
                            });
                        },
                        onPanUpdate: (details) {
                            // debugPrint("onPanUpdate");
                            setState(() {
                                RenderBox renderBox = context.findRenderObject() as RenderBox;
                                currentStroke.path.lineTo(details.localPosition.dx, details.localPosition.dy);
                                strokes.removeLast();
                                strokes.add(currentStroke);
                            });
                        },
                        onPanEnd: (details) {
                            debugPrint("onPanEnd");
                            // Nothing to do here...
                        },
                        child: Stack(
                            children: <Widget>[
                            CustomPaint(
                                size: Size.infinite,
                                painter: MyPainter(
                                    strokes: strokes,
                                    screenHeight: screenHeight
                                    )
                                )
                            ]
                        )
                    ),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                            margin: EdgeInsets.only(bottom: screenWidth * 0.05),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                    ElevatedButton(
                                        onPressed: (){ 
                                            setState((){ selectedColor = Colors.black; });
                                        },
                                        style: ElevatedButton.styleFrom(
                                            shape: const CircleBorder(),
                                            primary: Colors.black,
                                            fixedSize: Size(buttonSize, buttonSize),
                                            minimumSize: const Size(1, 1)
                                        ),
                                        child: const Text("", style: TextStyle(fontSize: 1)),
                                    ),
                                    ElevatedButton(
                                        onPressed: (){ 
                                            setState((){ selectedColor = Colors.green; });
                                        },
                                        style: ElevatedButton.styleFrom(
                                            shape: const CircleBorder(),
                                            primary: Colors.green,
                                            fixedSize: Size(buttonSize, buttonSize),
                                            minimumSize: const Size(1, 1)
                                        ),
                                        child: const Text(""),
                                    ),
                                    ElevatedButton(
                                        onPressed: (){ 
                                            setState((){ selectedColor = Colors.blue; });
                                        },
                                        style: ElevatedButton.styleFrom(
                                            shape: const CircleBorder(),
                                            primary: Colors.blue,
                                            fixedSize: Size(buttonSize, buttonSize),
                                            minimumSize: const Size(1, 1)
                                        ),
                                        child: const Text(""),
                                    ),
                                    ElevatedButton(
                                        onPressed: (){ 
                                            setState((){ selectedColor = Colors.orange; });
                                        },
                                        style: ElevatedButton.styleFrom(
                                            shape: const CircleBorder(),
                                            primary: Colors.orange,
                                            fixedSize: Size(buttonSize, buttonSize),
                                            minimumSize: const Size(1, 1)
                                        ),
                                        child: const Text(""),
                                    ),
                                    ElevatedButton(
                                        onPressed: (){ 
                                            setState((){ selectedColor = Colors.red; });
                                        },
                                        style: ElevatedButton.styleFrom(
                                            shape: const CircleBorder(),
                                            primary: Colors.red,
                                            fixedSize: Size(buttonSize, buttonSize),
                                            minimumSize: const Size(1, 1)
                                        ),
                                        child: const Text(""),
                                    ),
                                    Listener(
                                        onPointerDown: (details){ 
                                            setState((){ 
                                                if (strokes.length != 0) {
                                                    strokes.removeLast();
                                                }
                                            });
                                        },
                                        child: Icon(Icons.undo_rounded, color: Colors.black, size: buttonSize),

                                    ),
                                    Listener(
                                        onPointerDown: (details){ 
                                            setState((){ strokes.clear(); });
                                        },
                                        child: Icon(Icons.local_fire_department_rounded, color: Colors.black, size: buttonSize),

                                    ),
                                ]
                            )
                        )
                    ),
                ]
            ),
        );
    }
}

class MyPainter extends CustomPainter {
    MyPainter({required this.strokes, required this.screenHeight});


    // Keep track of the points tapped on the screen
    List<Stroke> strokes;
    // List<TouchPoints> pointsList;
    // List<Offset> offsetPoints = <Offset>[];

    double screenHeight;



    // This is where we can draw on canvas.
    @override
        void paint(Canvas canvas, Size size) {
            for (int i=0;i<strokes.length;i++) {
                // strokes[i].path.
                // if (strokes[i].path.length == 1) {
                //     debugPrint("Its a dot!");
                //     canvas.drawCircle(strokes[i][0].points, strokes[i][0].paint.strokeWidth/2, strokes[i][0].paint);
                // } else {
                debugPrint("its a line!");
                debugPrint(strokes[i].paint.color.toString());
                debugPrint(strokes[i].paint.strokeWidth.toString());
                debugPrint(strokes[i].paint.style.toString());
                debugPrint(strokes[i].paint.style.toString());

                debugPrint(strokes[i].path.toString());

                // Path path = Path();
                // path.moveTo(50, 50);
                // path.lineTo(50, 100);
                // path.lineTo(100, 100);
                // // path.lineTo(0, 0);
                // canvas.drawPath(path, Paint()..style = PaintingStyle.stroke); 
                canvas.drawPath(strokes[i].path, strokes[i].paint);
                    // for (int j=0;j<strokes[i].length-1;j++) {
                        // final controlPointX = xPrevious + (xValue - xPrevious) / 2;
                        // // HERE is the main line of code making your line smooth
                        // path.cubicTo(
                        //         controlPointX, yPrevious, controlPointX, yValue, xValue, yValue
                        // );

                        // canvas.

                    // }
            }
            // for (int i=0;i<pointsList.length-1;i++) {
            //     // Start or mid point in line
            //     if (!pointsList[i].end && !pointsList[i+1].end) {
            //         // Drawing line when two consecutive points are available
            //         canvas.drawLine(
            //             pointsList[i].points,
            //             pointsList[i+1].points,
            //             pointsList[i].paint
            //         );
            //     // End of the line or single tap
            //     } else if (!pointsList[i].end) {
            //         canvas.drawCircle(pointsList[i].points, pointsList[i].paint.strokeWidth/2, pointsList[i].paint);
            //     }
            // }
        }


    // Called when CustomPainter is rebuilt.
    // Returning true because we cant canvas to be rebuilt to reflect new changes.
    @override
        bool shouldRepaint(MyPainter oldDelegate) => true;
}
