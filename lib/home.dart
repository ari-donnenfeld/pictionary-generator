import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:math';
import 'draw.dart';
import 'src/easy.dart';
import 'src/medium.dart';
import 'src/hard.dart';

class MyHomePage extends StatefulWidget {
    const MyHomePage({Key? key}) : super(key: key);


    // This widget is the home page of your application. It is stateful, meaning
    // that it has a State object (defined below) that contains fields that affect
    // how it looks.

    // This class is the configuration for the state. It holds the values (in this
    // case the title) provided by the parent (in this case the App widget) and
    // used by the build method of the State. Fields in a Widget subclass are
    // always marked "final".

    @override
    State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with AutomaticKeepAliveClientMixin {
    String _word = "...";
    String _goStopText = "Go";
    String _dificultiesValue = "Easy";
    String sand = "0:00";
    String prevSand = "";
    Stopwatch stopwatch = Stopwatch();
    Timer? timer;
    bool _peek = false;

    List<String> dificulties = [
        "Easy",
        "Medium",
        "Hard"
    ];


    Random rnd = Random();

    void _getNewWord() {
        setState(() {
            /* debugPrint('Getting a new word...'); */
            if (_dificultiesValue == "Easy") {
                _word = easyArr[rnd.nextInt(easyArr.length)];
            } else if (_dificultiesValue == "Medium") {
                _word = medArr[rnd.nextInt(medArr.length)];
            } else {
                _word = hardArr[rnd.nextInt(hardArr.length)];
            }
        });
    }

    void _updatesand() {
        setState(() {
            int miliseconds = stopwatch.elapsedMilliseconds;
            String minutes = (miliseconds ~/ 60000).toString();
            String seconds = (miliseconds ~/ 1000 % 60).toString();
            sand = "$minutes:${seconds.padLeft(2, '0')}";
        });
    }

    void _goStop() {
        setState(() {
            // Check if counter is running
            if (stopwatch.isRunning) {
                // Stopwatch is running, it should be stopped
                stopwatch.stop();
                stopwatch.reset();
                timer?.cancel();
                prevSand = sand;
                _goStopText = "Go";
            } else {
                // Stopwatch is not running it should be started
                stopwatch.start();
                sand = "0:00";
                timer = Timer.periodic(const Duration(seconds: 1), (_) => _updatesand() );
                _goStopText = "Stop";
            }
        });
    }

    @override
    Widget build(BuildContext context) {
        // Setup Relative Sizes
        double screenWidth = MediaQuery.of(context).size.width;
        /* double screenHeight = MediaQuery.of(context).size.height; */


        // This method is rerun every time setState is called, for instance as done
        // by the _getNewWord method above.
        //
        // The Flutter framework has been optimized to make rerunning build methods
        // fast, so that you can just rebuild anything that needs updating rather
        // than having to individually change instances of widgets.
        return Scaffold(
            body: Stack(
                children: [
                    Align( // Draw button
                        alignment: const Alignment(1, 0),
                        child: Container(
                            margin: EdgeInsets.only(right: screenWidth * 0.03),
                            child: Listener(
                                onPointerDown: (details) {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => CanvasPainting(stopwatch)),);
                                },
                                child: Icon(Icons.draw, color: const Color.fromRGBO(50, 50, 50, 0.5), size: screenWidth * 0.1),
                            )
                        )
                    ),
                    Positioned( // Last
                        top: screenWidth * 0.05,
                        left: screenWidth * 0.05,
                        child: Row(
                                children: [Text("Last:  ", 
                                    style: TextStyle(fontSize: screenWidth * 0.05)),
                                    Text(prevSand, style: TextStyle(fontSize: screenWidth * 0.06, fontWeight: FontWeight.bold)),
                                ]
                        )
                    ),
                    Positioned( // Dificulty
                        top: screenWidth * 0.02,
                        right: screenWidth * 0.05,
                        child : DropdownButton(
                                value: _dificultiesValue,
                                icon: const Icon(Icons.keyboard_arrow_down),
                                itemHeight: screenWidth * 0.1,
                                iconSize: screenWidth * 0.08,
                                items: dificulties.map((String items) {
                                    return DropdownMenuItem(
                                            value: items,
                                            child: Text(items, 
                                                style: TextStyle(fontSize: screenWidth * 0.04)),
                                    );
                                }).toList(),
                                onChanged: (String? newValue) {
                                    setState(() {
                                        _dificultiesValue = newValue!;
                                    });
                                },
                            ),
                    ),
                    // Center is a layout widget. It takes a single child and positions it
                    // in the middle of the parent.
                    Center(
                        child: Column(
                            // Column is also a layout widget. It takes a list of children and
                            // arranges them vertically. By default, it sizes itself to fit its
                            // children horizontally, and tries to be as tall as its parent.
                            //
                            // Invoke "debug painting" (press "p" in the console, choose the
                            // "Toggle Debug Paint" action from the Flutter Inspector in Android
                            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
                            // to see the wireframe for each widget.
                            //
                            // Column has various properties to control how it sizes itself and
                            // how it positions its children. Here we use mainAxisAlignment to
                            // center the children vertically; the main axis here is the vertical
                            // axis because Columns are vertical (the cross axis would be
                            // horizontal).
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                                Text(
                                    stopwatch.isRunning ? "Time:" : "Your word is:",
                                    style: TextStyle(fontSize: screenWidth * 0.06),
                                ),
                                Text(
                                    stopwatch.isRunning ? _peek ? _word: sand : _word,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: screenWidth * 0.1, fontWeight: FontWeight.bold),
                                ),
                            ],
                        ),
                    ),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: 
                        Container(
                            margin: EdgeInsets.only(bottom: screenWidth * 0.05),
                            child : Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                    Listener(
                                        onPointerDown: (details) {
                                            /* _increaseCounterWhilePressed(); */
                                            setState(() {_peek = true; });
                                        },
                                        onPointerUp: (details) {
                                            setState(() {_peek = false; });
                                        },
                                        child: ElevatedButton(
                                            onPressed: stopwatch.isRunning ? (){} : _getNewWord,
                                            child: Text(stopwatch.isRunning ? 'Peek' : 'Generate', 
                                                style: TextStyle(fontSize: screenWidth * 0.1),
                                            ),
                                        ),
                                    ),
                                    ElevatedButton(
                                        onPressed: _goStop,
                                        child: Text(_goStopText, 
                                            style: TextStyle(fontSize: screenWidth * 0.1),
                                        ),
                                    ),
                                ],
                                
                            )
                        )
                    )
                ]
            )
        );
    }

    // Prevent state reset when navigating away
    @override
    bool get wantKeepAlive => true;
}
