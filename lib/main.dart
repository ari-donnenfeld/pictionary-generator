import 'package:flutter/material.dart';
import 'home.dart';
import 'dart:ui';

void main() {
    runApp(const MyApp());
}


// Allow mouse to work as touch
class AppScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}



class MyApp extends StatelessWidget {
    const MyApp({Key? key}) : super(key: key);

    // This widget is the root of your application.
    @override
        Widget build(BuildContext context) {
            return MaterialApp(
                    title: 'Pictionary',
                    theme: ThemeData(
                        // This is the theme of your application.
                        //
                        // Try running your application with "flutter run". You'll see the
                        // application has a blue toolbar. Then, without quitting the app, try
                        // changing the primarySwatch below to Colors.green and then invoke
                        // "hot reload" (press "r" in the console where you ran "flutter run",
                        // or simply save your changes to "hot reload" in a Flutter IDE).
                        // Notice that the counter didn't reset back to zero; the application
                        // is not restarted.
                        primarySwatch: Colors.deepPurple,
                    ),
                    scrollBehavior: AppScrollBehavior(),
                    home: const Main(),
                    );
        }
}

class Main extends StatelessWidget {

    const Main({Key? key}) : super(key: key);


    @override
    Widget build(BuildContext context) {
        // Setup Relative Sizes
        double screenWidth = MediaQuery.of(context).size.width;
        double screenHeight = MediaQuery.of(context).size.height;
        return Scaffold(
            appBar: AppBar(
                // Here we take the value from the MyHomePage object that was created by
                // the App.build method, and use it to set our appbar title.
                title: Text("Pictionary", style: TextStyle(fontSize: screenWidth * 0.06)),
                centerTitle: true,
                toolbarHeight: screenHeight * 0.08
            ),
            body: const Center(
                child: MyHomePage(),
            ),
        );
    }

}

