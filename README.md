# Pictionary Generator

A Pictionary-ish Word Generator app so that you can take the game wherever you go.

Chose from Easy, Medium or Hard words so its fun for all ages.

Includes a stopwatch, so that you can play by your own rules.

Though a whiteboard is recommended, Pictionary Generator comes with a simple canvas for you to draw on for when you only have your phone.

## Screenshots

![Easy Word](/metadata/en-US/images/phoneScreenshots/screenshot-easy.png) ![Timer Running](/metadata/en-US/images/phoneScreenshots/screenshot-easy-timer.png)

![Peek](/metadata/en-US/images/phoneScreenshots/screenshot-easy-peek.png) ![Drawing](/metadata/en-US/images/phoneScreenshots/screenshot-easy-drawing.png)

![Hard](/metadata/en-US/images/phoneScreenshots/screenshot-hard.png)

## Disclaimer

Please note: not all words have been vetted yet, this means that some words might be in the incorrect category, be nonsense or swear words. Feel free to report them.
